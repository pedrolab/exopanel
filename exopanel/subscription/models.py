from django.db import models

from django.utils.translation import gettext as _

import datetime

class BitstreamProvider(models.Model):

    name = models.CharField(
        verbose_name='Name',
        max_length=100,
    )

    description = models.TextField(
        _('Description'),
        max_length=300,
        blank=True,
    )

    service_vlan_id = models.IntegerField(
        verbose_name='Service VLAN ID',
        null=True,
        blank=True,
    )

    customer_service_vlan_id = models.IntegerField(
        verbose_name='Customer Service VLAN ID',
        null=True,
        blank=True,
    )

    customer_service_pcp = models.IntegerField(
        verbose_name='Customer Service PCP',
        null=True,
        blank=True,
    )

class BitstreamService(models.Model):

    # Instance of a bitstream

    provider = models.ForeignKey(
        BitstreamProvider,
        verbose_name='Provider',
        on_delete=models.CASCADE,
    )

    idont = models.IntegerField(
        verbose_name='IdONT',
        null=True,
        blank=True,
    )

    customer_vlan_id = models.IntegerField(
        verbose_name='Customer VLAN ID',
        null=True,
        blank=True,
    )

class Service(models.Model):

    # example of service: membership

    name = models.CharField(
        _('name'),
        max_length=50,
        unique=True
    )

    cost = models.DecimalField(
        _('cost'),
        default=0,
        max_digits=8,
        decimal_places=2,
    )

    description = models.TextField(
        _('Description'),
        max_length=300,
        blank=True,
    )

    class Meta:
        verbose_name = _('Service')
        verbose_name_plural = _('Services')

    def __str__(self):
        return '{} - {}€'.format(self.name, self.cost)

class Subscriber(models.Model):

    active = models.BooleanField(
        _('Active'),
        default=True,
        help_text=_(
            'The subscription is active'
        ),
    )

    identity = models.ForeignKey(
        'identity.Identity',
        verbose_name=_('Identity'),
        on_delete=models.CASCADE
    )

    service = models.ForeignKey(
        Service,
        verbose_name=_('Service'),
        on_delete=models.CASCADE
    )

    bitstream = models.ForeignKey(
        BitstreamService,
        null=True,
        blank=True,
        verbose_name=_('Bitstream'),
        on_delete=models.CASCADE
    )

    # TODO: check on data entry that start_subs should be greater or equal than end_subs
    # src http://stackoverflow.com/questions/2029295/django-datefield-default-options/2030142#2030142
    start_date = models.DateField(
        _('Start subscription date'),
        default=datetime.date.today,
    )

    # optional date
    # src http://stackoverflow.com/questions/11351619/how-to-make-djangos-datetimefield-optional/11351661#11351661
    end_date = models.DateField(
        _('End subscription date'),
        null=True,
        blank=True
    )

    notes = models.TextField(
        _('Notes'),
        max_length=300,
        blank=True,
    )

    count_netifaces = models.IntegerField(
        _('Number of interfaces'),
        blank=True,
        null=True,
        editable=False,
        help_text=_('To update this field you have to explicitly save this activity'),
    )

    def save(self):
        self.count_netifaces = self.network_interface_set.count()
        super(Subscriber, self).save()

    class Meta:
        verbose_name = _('Subscriber')
        verbose_name_plural = _('Subscribers')

    def __str__(self):
        return "{} - {}".format(self.identity, self.service)
